<!-- generar_pdf.php -->
<?php

// require_once("dompdf/dompdf_config.inc.php");
require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;
use Dompdf\Options;



$html ="formato.html";

$dompdf = new Dompdf();

$dompdf->set_option('chroot','/var/www/html/dibujar_pdf/formato');
echo $dompdf->get_option('chroot');


//Cargamos nuestro código HTML
$dompdf->load_html(file_get_contents( 'formato/formato.html' ));

//Hacemos la conversion de HTML a PDF
$dompdf->render();

//El nombre con el que deseamos guardar el archivo generado
$dompdf->stream("nombre");

?>